﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodefirstApplication
{
    public class FptSchoolContext : DbContext 
    {
        //base() > mac dinh ket noi localDB
        //base("tencuaDB") > db local sql Express co san
        //base("name=ten cua connection string")
        public FptSchoolContext() : base("name=FptDbCodeFirst")
        {
            Database.SetInitializer<FptSchoolContext>(new FptSchoolDbInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new StudentConfiguarions());
            modelBuilder.Entity<Teacher>()
                .ToTable("TeacherTable");
            modelBuilder.Entity<Teacher>()
                .MapToStoredProcedures();
            base.OnModelCreating(modelBuilder);
        }
        public DbSet<Student> Students { get; set; }
        public DbSet<Grade> Grades { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<StudentAddress> StudentAddresses { get; set; }
    }
}
