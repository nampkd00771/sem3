﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace CodefirstApplication
{
    public class FptSchoolDbInitializer: DropCreateDatabaseAlways<FptSchoolContext>
    {
        protected override void Seed(FptSchoolContext context)
        {
            IList<Grade> grades = new List<Grade>();

            grades.Add(new Grade() { GradeName = "G1", Section = "A" });
            grades.Add(new Grade() { GradeName = "G2", Section = "B" });
            grades.Add(new Grade() { GradeName = "G3", Section = "C" });
            context.Grades.AddRange(grades);
            base.Seed(context);
        }
    }
}
